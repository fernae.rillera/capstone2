// Objects


let student = {
	name : "brandon",
	age: 21,
	address: {
		stName: "Oxfort St.",
		barangay: "Brgy. 121",
		city: "Makati City"
	},
	favoriteFood:["Sinigang", "Tinola", "Fried Chicken"],
	mother: {
		name: "Eloisa",
		age: 62,
		address: {
			stName: "Oxfort St.",
		barangay: "Brgy. 121",
		city: "Makati City"
		}
	}
}


// tou access a data inside an obejct, we'll use the dot notation


student.favoriteFood.forEach(function(food)){
	console.log(food);
}