// console.log("hi");

const openBtn = document.getElementById('open-acct');
openBtn.addEventListener('click', function(){
	openBtn.parentElement.style.display = "none";
	openBtn.parentElement.nextElementSibling.classList.remove('d-none');
})

const submitBtn = document.getElementById('submit-btn');

let customer = {
	debit: 0,
	credit: 0,
	creditLimit: 50000,
	status: 'Active'
}

// Main-page
	let mainPage= document.getElementById('main-page');

submitBtn.addEventListener('click', function(){
	let customerName = submitBtn.previousElementSibling.lastElementChild.value;
	openBtn.parentElement.nextElementSibling.classList.add('d-none');


	mainPage.classList.remove('d-none');

	// title
	let mainTitle = document.createElement("h1");
	mainTitle.textContent = "Welcome " + customerName +"!";
	mainTitle.style.textAlign = "center";

	// insertBefore
	mainPage.insertBefore(mainTitle, mainPage.firstElementChild);

	// insert Below
	// mainPage.appendChild(mainTitle);

	// Debit span
	debit();
	// mainPage.lastElementChild.firstElementChild.firstElementChild.firstElementChild.textContent = customer.debit;

	// update the credit span
	credit();

})

const depositBtn = document.getElementById('deposit');
depositBtn.addEventListener('click', function(){
	let deposit = mainPage.lastElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.value;
	// Remember: when we capture a data form an input, it will automatically be transformed into a string no matter what the type we set is.
	// to fix that, we'll use parse.
	customer.debit += parseInt(deposit);
	mainPage.lastElementChild.firstElementChild.firstElementChild.firstElementChild.textContent = customer.debit;
});
depositBtn.previousElementSibling.addEventListener('click', function(){
	let withdraw = mainPage.lastElementChild.firstElementChild.firstElementChild.nextElementSibling.firstElementChild.value;
	if (withdraw>customer.debit){
		alert("Insufficient Funds")
	}else {
		customer.debit -= parseInt(withdraw);
		mainPage.lastElementChild.firstElementChild.firstElementChild.firstElementChild.textContent = customer.debit;
	}
});



const payBtn = document.getElementById('pay');
	payBtn.parentElement.firstElementChild.firstElementChild.textContent = customer.credit;
	payBtn.parentElement.firstElementChild.nextElementSibling.firstElementChild.textContent= customer.creditLimit;


// swipe

payBtn.previousElementSibling.addEventListener('click', function(){

	let swipe = payBtn.parentElement.firstElementChild.nextElementSibling.nextElementSibling.firstElementChild.value;

	if (swipe > customer.creditLimit){
		alert("You can't Swipe Higher than Credit Limit")

	}


	else{
		customer.credit += parseInt(swipe);
		customer.creditLimit -= parseInt(swipe);
		payBtn.parentElement.firstElementChild.firstElementChild.textContent = customer.credit;
		payBtn.parentElement.firstElementChild.nextElementSibling.firstElementChild.textContent= customer.creditLimit;
	}

});



// pay credit
payBtn.addEventListener('click', function(){

		let pay = payBtn.parentElement.firstElementChild.nextElementSibling.nextElementSibling.firstElementChild.value;
	

		if (customer.credit < 0){
			customer.creditLimit += parseInt(pay);
			payBtn.parentElement.firstElementChild.nextElementSibling.firstElementChild.textContent= customer.creditLimit;
		}


		// customer.credit -= parseInt(pay);
		// customer.creditLimit += parseInt(pay);
		// payBtn.parentElement.firstElementChild.firstElementChild.textContent = customer.credit;
		// payBtn.parentElement.firstElementChild.nextElementSibling.firstElementChild.textContent= customer.creditLimit;




	});

	
	// update the debit UI
	function debit(){
		mainPage.lastElementChild.firstElementChild.firstElementChild.firstElementChild.textContent = customer.debit;
	};

	// update the debit UI
	function credit(){
		mainPage.lastElementChild.firstElementChild.nextElementSibling.firstElementChild.firstElementChild.textContent = customer.credit;
	}

	function creditLimit(){
		mainPage.lastElementChild.firstElementChild.nextElementSibling.firstElementChild.nextElementSibling.firstElementChild.textContent = customer.creditLimit;
	}